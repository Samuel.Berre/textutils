package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) throws Exception {
			if(text == "") {
				return " ".repeat(width);
			}
			if((width % 2 == 0 && text.length() % 2 != 0) || (width % 2 != 0 && text.length() % 2 == 0)) {
				throw new Exception("Text length and width both need to be even or odd");
			}
			if(width < text.length()) {
				return "Please provide a width greater or equal to text length";
			}
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			if(text == "") {
				return " ".repeat(width);
			}
			if(width < text.length()) {
				return "Please provide a width greater or equal to text length";
			}
			return " ".repeat(width-text.length()) + text;
		}

		public String flushLeft(String text, int width) {
			if(text == "") {
				return " ".repeat(width);
			}
			if(width < text.length()) {
				return "Please provide a width greater or equal to text length";
			}
			return text + " ".repeat(width-text.length());
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() throws Exception {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" AA ", aligner.center("AA", 4));
		assertEquals("Please provide a width greater or equal to text length", aligner.center("AAA", 1));
		assertEquals("Please provide a width greater or equal to text length", aligner.center("A", -5));
		assertEquals("   ", aligner.center("", 3));
		int thrown = 0;
		try {
			aligner.center("A", 2);
		} catch (Exception e) {
			thrown++;
		}
		try {
			aligner.center("AA", 5);
		} catch (Exception e) {
			thrown++;
		}
		assertTrue(thrown == 2);
	}
	
	@Test
	void flushRight() {
		assertEquals("   A", aligner.flushRight("A", 4));
		assertEquals("Please provide a width greater or equal to text length", aligner.flushRight("A", -5));
	}
	
	@Test
	void flushLeft() {
		assertEquals("A   ", aligner.flushLeft("A", 4));
		assertEquals("Please provide a width greater or equal to text length", aligner.flushLeft("A", -5));
	}
	@Test
	void jusifty() {
		assertEquals("A   A", aligner.flushLeft("A A", 5));
		assertEquals("A   A   A", aligner.flushLeft("A A A", 9));
		assertEquals("A   A  A", aligner.flushLeft("A A A", 8));
	}
	@Test
	void 
}
